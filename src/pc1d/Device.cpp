/* PC1D Semiconductor Device Simulator
Copyright (C) 2003 University of New South Wales
Authors: Paul A. Basore, Donald A. Clugston

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "stdafx.h"
#include "PhysicalConstants.h"
#include "ProgramLimits.h"
#include "mathstat.h"	// CMath
#include "physics.h"	// CPhysics
#include "device.h"

#include "path.h"
#include "AscFile.h"
#include "PC1D.H"
#include "protocol.h"
#include "useful.h"

using namespace ::PC1DGrid::Protocol;

//BUG: Fix references to PC1D

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

const WORD DEVICEFILE_VERSION=1;
const WORD MATERIALFILE_VERSION=3;  // PC1D 4.2 used 0 PC1D 4.5 used 1 PC1D 5.0.1 used 2

const CString DEVICE_EXT="dev";
const CString MATERIAL_EXT="mat";
const CString REFLECTANCE_EXT="ref";
const CString INDEX_EXT="inr";
const CString ABSORPTION_EXT="abs";
const CString DOPING_EXT="dop";

/////////////////////////////////////////////////////////
// CMobility (Two per Material)

IMPLEMENT_SERIAL(CMobility, CObject, 0)

CMobility::CMobility() { }

void CMobility::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		ar << Fixed << Max << MajMin << MajNref << MajAlpha;
		ar << MinMin << MinNref << MinAlpha << B1 << B2 << B3 << B4 << Vmax;
	}
	else
	{
		ar >> Fixed >> Max >> MajMin >> MajNref >> MajAlpha;
		ar >> MinMin >> MinNref >> MinAlpha >> B1 >> B2 >> B3 >> B4 >> Vmax;
	}
}

void CMobility::Copy(CMobility* pMob)
{
	Fixed = pMob->Fixed; Max = pMob->Max; MajMin = pMob->MajMin;
	MajNref = pMob->MajNref; MajAlpha = pMob->MajAlpha; MinMin = pMob->MinMin; 
	MinNref = pMob->MinNref; MinAlpha = pMob->MinAlpha; 
	B1 = pMob->B1; B2 = pMob->B2; B3 = pMob->B3; B4 = pMob->B4; Vmax = pMob->Vmax;
}

///////////////////////////////////////////////////////////////////////////
// CDiffusion (Four per Region)

IMPLEMENT_SERIAL(CDiffusion, CObject, 0)

CDiffusion::CDiffusion()
{
	Initialize(NULL);
}

void CDiffusion::Initialize(CRegion* pR)
{
	m_pRegion = pR;
	m_Enable = FALSE; m_Type = N_TYPE; m_Npeak = 1e20; 
	m_Depth = 1e-4; m_Xpeak = 0; m_Profile = ERFC_PROFILE;
}

void CDiffusion::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	WORD ver_num;
	if (ar.IsStoring())
	{
		ver_num=1;
		ar << (WORD)ver_num;
		ar << (WORD)m_Enable << (WORD)m_Type << (WORD)m_Profile;
		ar << m_Npeak << m_Depth << m_Xpeak;
	}
	else
	{
		ar >> (WORD &)ver_num;
		if (ver_num != 1) 
		{
			AfxThrowArchiveException(CArchiveException::badIndex);
			return;
		}
		ar >> (WORD&)m_Enable >> (WORD&)m_Type >> (WORD&)m_Profile;
		ar >> m_Npeak >> m_Depth >> m_Xpeak;
	}
}

BOOL CDiffusion::SetDiffusion(CString title)
{
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// CMaterial (One per Region)

IMPLEMENT_SERIAL(CMaterial, CObject, 0)

void CMaterial::SetFileVersion(int PC1DVersion)
{
	// Material file version:
	// PC1D 4.2 used 0 PC1D 4.3-4.6 used 1 PC1D 5.0.1 used 2
	if (PC1DVersion<43) m_FileVersion=0;
	if (PC1DVersion<50) m_FileVersion=1;
					// fileversion 2 is defunct; was 5_internal_1.
	else m_FileVersion=3;
}

// the earliest version it could be saved as
int CMaterial::GetFileVersion()
{
	// Material file version:
	// PC1D 4.2 used 0 PC1D 4.3-4.6 used 1 PC1D 5.0.1 used 2
	if (!m_HurkxEnable) {
		// if we're using hurkx, has to be ver 5 at least, don't even try
		if (m_FileVersion==0) return 40;
		if (m_FileVersion<2) return 43;
	}
	return 50;	// fileversion 2 & 3 both are 5.0
}

void CMaterial::Initialize()
{
	m_Filename.Empty();
	SetFileVersion(PC1DVERSION);
	m_bModified = FALSE; 
	m_FixedMobility = FALSE;
	m_Elec.Fixed = 1200; m_Elec.Max = 1417; m_Elec.MajMin = 60.0; m_Elec.MajNref = 9.64E16;
	m_Elec.MajAlpha = 0.664; m_Elec.MinMin = 160; m_Elec.MinNref = 5.6e16; 
	m_Elec.MinAlpha = 0.647; m_Elec.B1 = -0.57; m_Elec.B2 = -2.33; m_Elec.B3 = 2.4; 
	m_Elec.B4 = -0.146; m_Elec.Vmax = 1e7;
	m_Hole.Fixed = 400; m_Hole.Max = 470; m_Hole.MajMin = 37.4; m_Hole.MajNref = 2.82e17;
	m_Hole.MajAlpha = 0.642; m_Hole.MinMin = 155; m_Hole.MinNref = 1.0E17;
	m_Hole.MinAlpha = 0.9; m_Hole.B1 = -0.57; m_Hole.B2 = -2.23; m_Hole.B3 = 2.4;
	m_Hole.B4 = -0.146; m_Hole.Vmax = 1e7;
	m_BandGap = 1.124; m_Affinity = 4.05; m_NcNv = 1.06;  m_Permittivity = 11.9;
	m_ni200 = 4.7e4; m_ni300 = 1.0e10; m_ni400 = 5.6e12;
	m_Cn = 2.2e-31; m_Cp = 9.9e-32; m_Cnp = 1.66e-30; 
	m_BB = 9.5e-15; // Ref: H.Schlangenotto et. al. Physica Status Solidi A21, 357-367 (1974).
	m_BulkNrefN = 1e15; m_BulkNalphaN = 0;
	m_BulkNrefP = 1e15; m_BulkNalphaP = 0;
	m_BulkTalpha = 0; m_BulkEref = 3.7E5;
	m_SurfNrefN = 1e18; m_SurfNalphaN = 0;
	m_SurfNrefP = 1e18; m_SurfNalphaP = 0;
	m_SurfTalpha = 0; m_BulkEgamma = 3.46;
	m_BGNnNref = 1.4e17; m_BGNnSlope = 0.014;
	m_BGNpNref = 1.4e17; m_BGNpSlope = 0.014;
	m_IndexFilename.Empty(); m_IndexExternal = FALSE;
	m_FixedIndex = 3.58;
	m_AbsFilename.Empty(); m_AbsExternal = FALSE;
	m_AbsEd1 = 3.177; m_AbsEd2 = 4.00; m_AbsEi1 = 1.102; m_AbsEi2 = 2.489;
	m_AbsEp1 = 0.01826; m_AbsEp2 = 0.06494; m_AbsAd1 = 1.29e6;
	m_AbsAd2 = 3.746e4; m_AbsA11 = 1.468e3; m_AbsA12 = 1.561e3;
	m_AbsA21 = 2.890e4; m_AbsA22 = 8.797e4; m_AbsTcoeff = 4.73e-4;
	m_AbsToffset = 636;
	m_nAbsorb = m_nIndex = 0;
	for (int k=0; k<MAX_WAVELENGTHS; k++)
	{
		m_AbsLambda[k] = m_Absorption[k] = 0;
		m_IdxLambda[k] = 0; m_Index[k] = 1;
	}
	m_FreeCarrEnable=TRUE;
	m_FreeCarrCoeffN=2.6e-27;	m_FreeCarrPowerN=3; // use Schmidt's Si data, Green's fit.
	m_FreeCarrCoeffP=2.7e-24;   m_FreeCarrPowerP=2;
	m_HurkxFgamma = 3.688e5; 	// V/cm use Hurkx paper.
	m_HurkxPrefactor = 6.14;	// 2*sqrt(3*PI)
	m_HurkxEnable=TRUE;
}

void CMaterial::Copy(CMaterial* pM)
{
	m_Filename = pM->m_Filename;
	m_FileVersion= pM->m_FileVersion;
	m_bModified = pM->m_bModified;

	m_FixedMobility = pM->m_FixedMobility;
	m_Elec.Copy(&pM->m_Elec);
	m_Hole.Copy(&pM->m_Hole);
	m_BandGap = pM->m_BandGap; m_Affinity = pM->m_Affinity; m_NcNv = pM->m_NcNv;
	m_Permittivity = pM->m_Permittivity;
	m_ni200 = pM->m_ni200; m_ni300 = pM->m_ni300; m_ni400 = pM->m_ni400;
	m_Cn = pM->m_Cn; m_Cp = pM->m_Cp; m_Cnp = pM->m_Cnp; m_BB = pM->m_BB;
	m_BulkNrefN = pM->m_BulkNrefN; m_BulkNalphaN = pM->m_BulkNalphaN;
	m_BulkNrefP = pM->m_BulkNrefP; m_BulkNalphaP = pM->m_BulkNalphaP;
	m_BulkTalpha = pM->m_BulkTalpha; m_BulkEref = pM->m_BulkEref;
	m_SurfNrefN = pM->m_SurfNrefN; m_SurfNalphaN = pM->m_SurfNalphaN;
	m_SurfNrefP = pM->m_SurfNrefP; m_SurfNalphaP = pM->m_SurfNalphaP;
	m_SurfTalpha = pM->m_SurfTalpha; m_BulkEgamma = pM->m_BulkEgamma;
	m_BGNnNref = pM->m_BGNnNref; m_BGNnSlope = pM->m_BGNnSlope;
	m_BGNpNref = pM->m_BGNpNref; m_BGNpSlope = pM->m_BGNpSlope;
	m_IndexFilename = pM->m_IndexFilename; m_IndexExternal = pM->m_IndexExternal;
	m_FixedIndex = pM->m_FixedIndex;
	m_AbsFilename = pM->m_AbsFilename; m_AbsExternal = pM->m_AbsExternal;
	m_AbsEd1 = pM->m_AbsEd1; m_AbsEd2 = pM->m_AbsEd2; m_AbsEi1 = pM->m_AbsEi1;
	m_AbsEi2 = pM->m_AbsEi2; m_AbsEp1 = pM->m_AbsEp1; m_AbsEp2 = pM->m_AbsEp2;
	m_AbsAd1 = pM->m_AbsAd1; m_AbsAd2 = pM->m_AbsAd2; m_AbsA11 = pM->m_AbsA11;
	m_AbsA12 = pM->m_AbsA12; m_AbsA22 = pM->m_AbsA22;
	m_AbsTcoeff = pM->m_AbsTcoeff; m_AbsToffset = pM->m_AbsToffset;
	m_nAbsorb = pM->m_nAbsorb; m_nIndex = pM->m_nIndex;
	for (int k=0; k<MAX_WAVELENGTHS; k++)
	{
		m_AbsLambda[k] = pM->m_AbsLambda[k]; m_Absorption[k] = pM->m_Absorption[k];
		m_IdxLambda[k] = pM->m_IdxLambda[k]; m_Index[k] = pM->m_Index[k];
	}
	m_FreeCarrCoeffN=pM->m_FreeCarrCoeffN;	m_FreeCarrPowerN=pM->m_FreeCarrPowerN;
	m_FreeCarrCoeffP=pM->m_FreeCarrCoeffP;  m_FreeCarrPowerP=pM->m_FreeCarrPowerP;
	m_HurkxEnable = pM->m_HurkxEnable;
	m_HurkxFgamma = pM->m_HurkxFgamma;
	m_HurkxPrefactor=pM->m_HurkxPrefactor;
}

CMaterial::CMaterial()
{
	Initialize();
}

CMaterial::~CMaterial()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMaterial serialization

void CMaterial::Serialize(CArchive& ar)
{
	WORD version;
	CObject::Serialize(ar);
	m_Elec.Serialize(ar); m_Hole.Serialize(ar);
	if (ar.IsStoring())
	{
//		version=MATERIALFILE_VERSION;
		version=m_FileVersion;
		ar << CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.mat );
		if (version>0) {
			if (m_bModified) ar << (WORD)0xDC01; else ar << (WORD)0xDC00;
			ar << version;
		} else	ar << (WORD)m_bModified;
		ar << (WORD)m_FixedMobility;
		ar << m_Permittivity << m_BandGap << m_Affinity << m_NcNv;
		ar << m_ni200 << m_ni300 << m_ni400;
		ar << m_BGNnNref << m_BGNnSlope << m_BGNpNref << m_BGNpSlope;
		ar << m_Cn << m_Cp << m_Cnp << m_BB;
		ar << m_BulkNrefN << m_BulkNalphaN;
		ar << m_BulkNrefP << m_BulkNalphaP;
		ar << m_BulkTalpha << m_BulkEref << m_BulkEgamma;
		ar << m_SurfNrefN << m_SurfNalphaN;
		ar << m_SurfNrefP << m_SurfNalphaP;
		ar << m_SurfTalpha;

		ar << CPath::MinimumNecessaryFilename(m_IndexFilename, ((CPc1dApp *)AfxGetApp())->m_Path.inr );				
		ar << (WORD)m_IndexExternal << m_FixedIndex;
		ar << CPath::MinimumNecessaryFilename(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs );
		ar << (WORD)m_AbsExternal;
		
		ar << m_AbsEd1 << m_AbsEd2 << m_AbsEi1 << m_AbsEi2;
		ar << m_AbsEp1 << m_AbsEp2 << m_AbsAd1 << m_AbsAd2;
		ar << m_AbsA11 << m_AbsA12 << m_AbsA21 << m_AbsA22;
		ar << m_AbsTcoeff << m_AbsToffset;
		if (version>0) {
			ar << (WORD)m_FreeCarrEnable;
			ar << m_FreeCarrCoeffN << m_FreeCarrPowerN << m_FreeCarrCoeffP << m_FreeCarrPowerP;
		}
		if (version==2) ar << m_HurkxPrefactor << m_HurkxFgamma;
		if (version>2) {
			ar << (WORD)m_HurkxEnable << m_HurkxPrefactor << m_HurkxFgamma;
		}
	}
	else
	{		
		ar >> m_Filename; 
		ar >> (WORD&)m_bModified;
		if ((m_bModified & 0xFF00) == 0xDC00) { ar >> version; m_bModified &=0x00FF;} 
		else version=0;		
		m_FileVersion=version;
		ar >> (WORD&)m_FixedMobility;
		ar >> m_Permittivity >> m_BandGap >> m_Affinity >> m_NcNv;
		ar >> m_ni200 >> m_ni300 >> m_ni400;
		ar >> m_BGNnNref >> m_BGNnSlope >> m_BGNpNref >> m_BGNpSlope;
		ar >> m_Cn >> m_Cp >> m_Cnp >> m_BB;
		ar >> m_BulkNrefN >> m_BulkNalphaN;
		ar >> m_BulkNrefP >> m_BulkNalphaP;
		ar >> m_BulkTalpha >> m_BulkEref >> m_BulkEgamma;
		ar >> m_SurfNrefN >> m_SurfNalphaN;
		ar >> m_SurfNrefP >> m_SurfNalphaP;
		ar >> m_SurfTalpha;
		ar >> m_IndexFilename >> (WORD&)m_IndexExternal >> m_FixedIndex;
		ar >> m_AbsFilename >> (WORD&)m_AbsExternal;
		ar >> m_AbsEd1 >> m_AbsEd2 >> m_AbsEi1 >> m_AbsEi2;
		ar >> m_AbsEp1 >> m_AbsEp2 >> m_AbsAd1 >> m_AbsAd2;
		ar >> m_AbsA11 >> m_AbsA12 >> m_AbsA21 >> m_AbsA22;
		ar >> m_AbsTcoeff >> m_AbsToffset;
		if (version>0) {
			ar >> (WORD &)m_FreeCarrEnable ;
			ar >> m_FreeCarrCoeffN >> m_FreeCarrPowerN >> m_FreeCarrCoeffP >> m_FreeCarrPowerP;
		} else {
			m_FreeCarrEnable=FALSE;
			m_FreeCarrCoeffN=2.6e-27;	m_FreeCarrPowerN=3; // use Schmidt's Si data, Green's fit.
			m_FreeCarrCoeffP=2.7e-18*1e-6;  m_FreeCarrPowerP=2;
		}
		switch (version) {
		case 2:
			ar >> m_HurkxPrefactor >> m_HurkxFgamma;
			m_HurkxEnable=TRUE;
			break;
		case 0:
		case 1:
			m_HurkxFgamma = 3.688e5; 	// V/cm use Hurkx paper.
			m_HurkxPrefactor = 6.14;	// 2*sqrt(3*PI)
			m_HurkxEnable = FALSE;
			break;
		case 3:
		default:
			ar >> (WORD &)m_HurkxEnable >> m_HurkxPrefactor >> m_HurkxFgamma;
			break;
		}

		// finished reading from material file, now must act on the data...
		// Now load data from external files if required

		m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.mat );
		m_IndexFilename=CPath::MinimumNecessaryFilename(m_IndexFilename, ((CPc1dApp *)AfxGetApp())->m_Path.inr );
		m_AbsFilename=CPath::MinimumNecessaryFilename(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs );
		m_nAbsorb = MAX_WAVELENGTHS;
		if (m_AbsExternal)
			if (!CAscFile::Read(CPath::MakeFullPathname(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs ),
					m_nAbsorb, m_AbsLambda, m_Absorption))
				AfxMessageBox("Error reading Absorption file "+m_AbsFilename);
		m_nIndex = MAX_WAVELENGTHS;
		if (m_IndexExternal)
			if (!CAscFile::Read(CPath::MakeFullPathname(m_IndexFilename, ((CPc1dApp *)AfxGetApp())->m_Path.inr ),
					m_nIndex, m_IdxLambda, m_Index))
				AfxMessageBox("Error reading Refractive Index file "+m_IndexFilename);
	}
}

void CMaterial::Serialize(CArchive& ar, const ::PC1DGrid::Protocol::FileMap &fileMap)
{
	WORD version;
	CObject::Serialize(ar);
	m_Elec.Serialize(ar); m_Hole.Serialize(ar);
	if (ar.IsStoring())
	{
//		version=MATERIALFILE_VERSION;
		version=m_FileVersion;
		ar << CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.mat );
		if (version>0) {
			if (m_bModified) ar << (WORD)0xDC01; else ar << (WORD)0xDC00;
			ar << version;
		} else	ar << (WORD)m_bModified;
		ar << (WORD)m_FixedMobility;
		ar << m_Permittivity << m_BandGap << m_Affinity << m_NcNv;
		ar << m_ni200 << m_ni300 << m_ni400;
		ar << m_BGNnNref << m_BGNnSlope << m_BGNpNref << m_BGNpSlope;
		ar << m_Cn << m_Cp << m_Cnp << m_BB;
		ar << m_BulkNrefN << m_BulkNalphaN;
		ar << m_BulkNrefP << m_BulkNalphaP;
		ar << m_BulkTalpha << m_BulkEref << m_BulkEgamma;
		ar << m_SurfNrefN << m_SurfNalphaN;
		ar << m_SurfNrefP << m_SurfNalphaP;
		ar << m_SurfTalpha;

		ar << CPath::MinimumNecessaryFilename(m_IndexFilename, ((CPc1dApp *)AfxGetApp())->m_Path.inr );				
		ar << (WORD)m_IndexExternal << m_FixedIndex;
		ar << CPath::MinimumNecessaryFilename(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs );
		ar << (WORD)m_AbsExternal;
		
		ar << m_AbsEd1 << m_AbsEd2 << m_AbsEi1 << m_AbsEi2;
		ar << m_AbsEp1 << m_AbsEp2 << m_AbsAd1 << m_AbsAd2;
		ar << m_AbsA11 << m_AbsA12 << m_AbsA21 << m_AbsA22;
		ar << m_AbsTcoeff << m_AbsToffset;
		if (version>0) {
			ar << (WORD)m_FreeCarrEnable;
			ar << m_FreeCarrCoeffN << m_FreeCarrPowerN << m_FreeCarrCoeffP << m_FreeCarrPowerP;
		}
		if (version==2) ar << m_HurkxPrefactor << m_HurkxFgamma;
		if (version>2) {
			ar << (WORD)m_HurkxEnable << m_HurkxPrefactor << m_HurkxFgamma;
		}
	}
	else
	{		
		ar >> m_Filename; 
		ar >> (WORD&)m_bModified;
		if ((m_bModified & 0xFF00) == 0xDC00) { ar >> version; m_bModified &=0x00FF;} 
		else version=0;		
		m_FileVersion=version;
		ar >> (WORD&)m_FixedMobility;
		ar >> m_Permittivity >> m_BandGap >> m_Affinity >> m_NcNv;
		ar >> m_ni200 >> m_ni300 >> m_ni400;
		ar >> m_BGNnNref >> m_BGNnSlope >> m_BGNpNref >> m_BGNpSlope;
		ar >> m_Cn >> m_Cp >> m_Cnp >> m_BB;
		ar >> m_BulkNrefN >> m_BulkNalphaN;
		ar >> m_BulkNrefP >> m_BulkNalphaP;
		ar >> m_BulkTalpha >> m_BulkEref >> m_BulkEgamma;
		ar >> m_SurfNrefN >> m_SurfNalphaN;
		ar >> m_SurfNrefP >> m_SurfNalphaP;
		ar >> m_SurfTalpha;
		ar >> m_IndexFilename >> (WORD&)m_IndexExternal >> m_FixedIndex;
		ar >> m_AbsFilename >> (WORD&)m_AbsExternal;
		ar >> m_AbsEd1 >> m_AbsEd2 >> m_AbsEi1 >> m_AbsEi2;
		ar >> m_AbsEp1 >> m_AbsEp2 >> m_AbsAd1 >> m_AbsAd2;
		ar >> m_AbsA11 >> m_AbsA12 >> m_AbsA21 >> m_AbsA22;
		ar >> m_AbsTcoeff >> m_AbsToffset;
		if (version>0) {
			ar >> (WORD &)m_FreeCarrEnable ;
			ar >> m_FreeCarrCoeffN >> m_FreeCarrPowerN >> m_FreeCarrCoeffP >> m_FreeCarrPowerP;
		} else {
			m_FreeCarrEnable=FALSE;
			m_FreeCarrCoeffN=2.6e-27;	m_FreeCarrPowerN=3; // use Schmidt's Si data, Green's fit.
			m_FreeCarrCoeffP=2.7e-18*1e-6;  m_FreeCarrPowerP=2;
		}
		switch (version) {
		case 2:
			ar >> m_HurkxPrefactor >> m_HurkxFgamma;
			m_HurkxEnable=TRUE;
			break;
		case 0:
		case 1:
			m_HurkxFgamma = 3.688e5; 	// V/cm use Hurkx paper.
			m_HurkxPrefactor = 6.14;	// 2*sqrt(3*PI)
			m_HurkxEnable = FALSE;
			break;
		case 3:
		default:
			ar >> (WORD &)m_HurkxEnable >> m_HurkxPrefactor >> m_HurkxFgamma;
			break;
		}

		// finished reading from material file, now must act on the data...
		// Now load data from external files if required
		m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.mat );
		m_IndexFilename=CPath::MinimumNecessaryFilename(m_IndexFilename, ((CPc1dApp *)AfxGetApp())->m_Path.inr );
		m_AbsFilename=CPath::MinimumNecessaryFilename(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs );
		m_nAbsorb = MAX_WAVELENGTHS;
		if (m_AbsExternal)
			UnpackFromExternalFile(fileMap, std::string(m_AbsFilename), m_AbsLambda, m_Absorption, m_nAbsorb, MAX_WAVELENGTHS);

		m_nIndex = MAX_WAVELENGTHS;
		if (m_IndexExternal)
			UnpackFromExternalFile(fileMap, std::string(m_IndexFilename), m_IdxLambda, m_Index, m_nIndex, MAX_WAVELENGTHS);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMaterial implementation

BOOL CMaterial::OnMaterialOpen()
{
	BOOL FileOpen = TRUE;
	CString Ext = MATERIAL_EXT;
	CString Filter = "Material (*.mat)|*.mat|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_Filename, OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Open Material File";
	dlg.m_ofn.lpstrInitialDir = ((CPc1dApp *)AfxGetApp())->m_Path.mat;
	if (dlg.DoModal()==IDOK)
	{
		CFile f;
		if (!f.Open(dlg.GetPathName(), CFile::modeRead))
		{
			AfxMessageBox("Unable to open material file");
			return FALSE;
		}
		else 
		{
			CArchive ar(&f, CArchive::load); 
			Serialize(ar);
			m_Filename = dlg.GetPathName();
			m_Filename.MakeLower();
			m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.mat );
			m_bModified = FALSE;
			return TRUE;
		}
	}
	else return FALSE;
}

void CMaterial::OnMaterialSaveas()
{
	BOOL FileOpen = FALSE;
	CString Ext = MATERIAL_EXT;
	CString Filter = "Material (*.mat)|*.mat|PC1D 4.5 Material (*.mat)|*.mat|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_Filename, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Save Material File";
	dlg.m_ofn.lpstrInitialDir = ((CPc1dApp *)AfxGetApp())->m_Path.mat;	
	if (GetFileVersion()<50) dlg.m_ofn.nFilterIndex=2;
	else dlg.m_ofn.nFilterIndex=1;

	if (dlg.DoModal()==IDOK)
	{
		CFile f;
		if (!f.Open(dlg.GetPathName(), CFile::modeCreate | CFile::modeWrite))
		{
			AfxMessageBox("Unable to create material file");
		}
		else 
		{
			if (dlg.m_ofn.nFilterIndex==2) {
				SetFileVersion(45);
			} else {
				SetFileVersion(50);
			}
			CArchive ar(&f, CArchive::store); 
			Serialize(ar);
			m_Filename = dlg.GetPathName();
			m_Filename.MakeLower();
			m_bModified = FALSE;
		}
	}
}

BOOL CMaterial::SetMobilityModel()
{
	return TRUE;
}

BOOL CMaterial::SetMobilityFixed()
{
	return TRUE;
}

BOOL CMaterial::SetPermittivity()
{
	return TRUE;
}

BOOL CMaterial::SetBandStructure()
{
	return TRUE;
}

BOOL CMaterial::DoRecombinationDlg()
{
	return TRUE;
}
	

BOOL CMaterial::SetBandgapNarrowing()
{
	return TRUE;
}

BOOL CMaterial::DoOpticalDlg(int toppage)
{
	return TRUE;
}

BOOL CMaterial::SetAbsFile(CString path)
{
	BOOL FileOpen = TRUE;
	CString Ext = ABSORPTION_EXT;
	CString Filter = "Absorption (*."+Ext+")|*."+Ext+"|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_AbsFilename, OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Open Absorption Coefficients File";
	dlg.m_ofn.lpstrInitialDir = path;
	if (dlg.DoModal()==IDOK)
	{
		m_AbsFilename = dlg.GetPathName();
		m_AbsFilename.MakeLower();
		m_nAbsorb = MAX_WAVELENGTHS;
		if (!CAscFile::Read(CPath::MakeFullPathname(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs), m_nAbsorb, m_AbsLambda, m_Absorption))
			AfxMessageBox("Error reading Absorption file "+m_AbsFilename);
		m_AbsFilename=CPath::MinimumNecessaryFilename(m_AbsFilename, ((CPc1dApp *)AfxGetApp())->m_Path.abs );
		return TRUE;
	}
	else return FALSE;
}

double CMaterial::GetAbsorptionAtWavelength(double lambda, double T) const
{
	if (m_AbsExternal)
			return CMath::LogInterp(lambda, m_nAbsorb, m_AbsLambda, m_Absorption);
	else return CPhysics::Absorption(lambda, m_AbsEd1, m_AbsEd2, 
	    		m_AbsEi1, m_AbsEi2, m_AbsEp1, m_AbsEp2, 
	    		m_AbsAd1, m_AbsAd2, m_AbsA11, m_AbsA12,
	    		m_AbsA21, m_AbsA22, m_AbsTcoeff, m_AbsToffset, T);
}

double CMaterial::GetRefractiveIndexAtWavelength(double lambda) const
{
	if (m_IndexExternal) return CMath::LinearInterp(lambda, m_nIndex, m_IdxLambda, m_Index);
	else return m_FixedIndex;
}


/////////////////////////////////////////////////////////
// CRegion (Up to 5 per Device)

IMPLEMENT_SERIAL(CRegion, CObject, 0)

CRegion::CRegion()
{
	Initialize();
}

void CRegion::Initialize()
{
	m_Mat.Initialize();
	m_FrontDiff1.Initialize(this);
	m_FrontDiff2.Initialize(this);
	m_RearDiff1.Initialize(this);
	m_RearDiff2.Initialize(this);
	m_Thickness = 10E-4;
	m_FrontFilename.Empty(); m_FrontExternal = FALSE;
	m_RearFilename.Empty(); m_RearExternal = FALSE;
	m_BkgndDop = 1e16; m_BkgndType = P_TYPE;
	m_TauN = 1e-3; m_TauP = 1e-3; m_BulkEt = 0;
	m_FrontJo = FALSE;
	m_FrontSn = 0; m_FrontSp = 0; m_FrontEt = 0;
	m_RearJo = FALSE;
	m_RearSn = 0; m_RearSp = 0; m_RearEt = 0;
	m_nFront = m_nRear = 0;
	for (int k=0; k<MAX_ELEMENTS; k++)
	{
		m_FrontPosition[k] = 0; m_FrontDopingDonor[k] = m_FrontDopingAcceptor[k] = 1;
		m_RearPosition[k] = 0; m_RearDopingDonor[k] = m_RearDopingAcceptor[k] = 1;
	}
}

void CRegion::Copy(CRegion* pR)
{
	// External & Diffusion doping not copied to new regions
	// Sn and Sp not copied either, nor is Jo preference
	m_Mat.Copy(&pR->m_Mat);
	m_Thickness = pR->m_Thickness;
	m_BkgndDop = pR->m_BkgndDop; m_BkgndType = pR->m_BkgndType;	
	m_TauN = pR->m_TauN; m_TauP = pR->m_TauP; m_BulkEt = pR->m_BulkEt;
}

/////////////////////////////////////////////////////////////////////////////
// CRegion serialization

void CRegion::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	m_Mat.Serialize(ar);
	m_FrontDiff1.Serialize(ar);
	m_FrontDiff2.Serialize(ar);
	m_RearDiff1.Serialize(ar);
	m_RearDiff2.Serialize(ar);
	if (ar.IsStoring())
	{
		ar << m_Thickness;
		ar << CPath::MinimumNecessaryFilename(m_FrontFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop);
		ar << (WORD)m_FrontExternal;
		ar << CPath::MinimumNecessaryFilename(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop);
		ar << (WORD)m_RearExternal;		
		ar << m_BkgndDop << (WORD)m_BkgndType;
		ar << m_TauN << m_TauP << m_BulkEt;
		ar << m_FrontSn << m_FrontSp << m_FrontEt << (WORD)m_FrontJo;
		ar << m_RearSn << m_RearSp << m_RearEt << (WORD)m_RearJo;
	}
	else
	{
		ar >> m_Thickness;
		ar >> m_FrontFilename >> (WORD&)m_FrontExternal;
		ar >> m_RearFilename >> (WORD&)m_RearExternal;
		ar >> m_BkgndDop >> (WORD&)m_BkgndType;
		ar >> m_TauN >> m_TauP >> m_BulkEt;
		ar >> m_FrontSn >> m_FrontSp >> m_FrontEt >> (WORD&)m_FrontJo;
		ar >> m_RearSn >> m_RearSp >> m_RearEt >> (WORD&)m_RearJo;
		m_FrontFilename=CPath::MinimumNecessaryFilename(m_FrontFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop);
		m_RearFilename=CPath::MinimumNecessaryFilename(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop);
		m_nFront = MAX_ELEMENTS;
		if (m_FrontExternal)
		{
			if (!CAscFile::Read(CPath::MakeFullPathname(m_FrontFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop),
					m_nFront, m_FrontPosition, m_FrontDopingDonor, m_FrontDopingAcceptor))
				AfxMessageBox("Error reading Doping file "+m_FrontFilename);
			for (int k=0; k<m_nFront; k++) m_FrontPosition[k] *= 1E-4;	// um to cm
		}
		m_nRear = MAX_ELEMENTS;
		if (m_RearExternal)
		{
			if (!CAscFile::Read(CPath::MakeFullPathname(m_RearFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop),
			 		m_nRear, m_RearPosition, m_RearDopingDonor, m_RearDopingAcceptor))
				AfxMessageBox("Error reading Doping file "+m_RearFilename);
			for (int k=0; k<m_nRear; k++) m_RearPosition[k] *= 1E-4;	// um to cm
		}
	}
}

void CRegion::Serialize(CArchive& ar, const ::PC1DGrid::Protocol::FileMap &fileMap)
{
	CObject::Serialize(ar);
	m_Mat.Serialize(ar, fileMap);
	m_FrontDiff1.Serialize(ar);
	m_FrontDiff2.Serialize(ar);
	m_RearDiff1.Serialize(ar);
	m_RearDiff2.Serialize(ar);
	if (ar.IsStoring())
	{
		ar << m_Thickness;
		ar << CPath::MinimumNecessaryFilename(m_FrontFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop);
		ar << (WORD)m_FrontExternal;
		ar << CPath::MinimumNecessaryFilename(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop);
		ar << (WORD)m_RearExternal;		
		ar << m_BkgndDop << (WORD)m_BkgndType;
		ar << m_TauN << m_TauP << m_BulkEt;
		ar << m_FrontSn << m_FrontSp << m_FrontEt << (WORD)m_FrontJo;
		ar << m_RearSn << m_RearSp << m_RearEt << (WORD)m_RearJo;
	}
	else
	{
		ar >> m_Thickness;
		ar >> m_FrontFilename >> (WORD&)m_FrontExternal;
		ar >> m_RearFilename >> (WORD&)m_RearExternal;
		ar >> m_BkgndDop >> (WORD&)m_BkgndType;
		ar >> m_TauN >> m_TauP >> m_BulkEt;
		ar >> m_FrontSn >> m_FrontSp >> m_FrontEt >> (WORD&)m_FrontJo;
		ar >> m_RearSn >> m_RearSp >> m_RearEt >> (WORD&)m_RearJo;
		m_FrontFilename=CPath::MinimumNecessaryFilename(m_FrontFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop);
		m_RearFilename=CPath::MinimumNecessaryFilename(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop);
		m_nFront = MAX_ELEMENTS;
		if (m_FrontExternal)
		{
			if (!CAscFile::Read(CPath::MakeFullPathname(m_FrontFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop),
					m_nFront, m_FrontPosition, m_FrontDopingDonor, m_FrontDopingAcceptor))
				AfxMessageBox("Error reading Doping file "+m_FrontFilename);
			for (int k=0; k<m_nFront; k++) m_FrontPosition[k] *= 1E-4;	// um to cm
		}
		m_nRear = MAX_ELEMENTS;
		if (m_RearExternal)
		{
			if (!CAscFile::Read(CPath::MakeFullPathname(m_RearFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop),
			 		m_nRear, m_RearPosition, m_RearDopingDonor, m_RearDopingAcceptor))
				AfxMessageBox("Error reading Doping file "+m_RearFilename);
			for (int k=0; k<m_nRear; k++) m_RearPosition[k] *= 1E-4;	// um to cm
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRegion implementation

BOOL CRegion::SetRegionThickness()
{
	return TRUE;
}

BOOL CRegion::SetBackgroundDoping()
{
	return TRUE;
}

BOOL CRegion::DoRecombinationDlg(int regionnum, int toppage)
{   
	return TRUE;
}

BOOL CRegion::SetFrontDopingFile(CString path)
{
	BOOL FileOpen = TRUE;
	CString Ext = DOPING_EXT;
	CString Filter = "Doping (*."+Ext+")|*."+Ext+"|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_FrontFilename, OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Open Front Doping File";
	dlg.m_ofn.lpstrInitialDir = path;
	if (dlg.DoModal()==IDOK)
	{
		m_FrontFilename = dlg.GetPathName();
		m_FrontFilename.MakeLower();
		m_nFront = MAX_ELEMENTS;
		if (!CAscFile::Read(CPath::MakeFullPathname(m_FrontFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop), m_nFront, m_FrontPosition, m_FrontDopingDonor, m_FrontDopingAcceptor))
			AfxMessageBox("Error reading Doping file "+m_FrontFilename);
		for (int k=0; k<m_nFront; k++) m_FrontPosition[k] *= 1E-4;	// um to cm
		m_FrontFilename=CPath::MinimumNecessaryFilename(m_FrontFilename,((CPc1dApp *)AfxGetApp())->m_Path.dop);
		return TRUE;
	}
	else return FALSE;
}

BOOL CRegion::SetRearDopingFile(CString path)
{
	BOOL FileOpen = TRUE;
	CString Ext = DOPING_EXT;
	CString Filter = "Doping (*."+Ext+")|*."+Ext+"|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_RearFilename, OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Open Rear Doping File";
	dlg.m_ofn.lpstrInitialDir = path;
	if (dlg.DoModal()==IDOK)
	{
		m_RearFilename = dlg.GetPathName();
		m_RearFilename.MakeLower();
		m_nRear = MAX_ELEMENTS;
		if (!CAscFile::Read(CPath::MakeFullPathname(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop), m_nRear, m_RearPosition, m_RearDopingDonor, m_RearDopingAcceptor))
			AfxMessageBox("Error reading Doping file "+m_RearFilename);
		for (int k=0; k<m_nRear; k++) m_RearPosition[k] *= 1E-4;	// um to cm
		m_RearFilename=CPath::MinimumNecessaryFilename(m_RearFilename, ((CPc1dApp *)AfxGetApp())->m_Path.dop);
		return TRUE;
	}
	else return FALSE;
}

///////////////////////////////////////////////////////////////////////////
// CReflectance (Two per Device)

IMPLEMENT_SERIAL(CReflectance, CObject, 0)

CReflectance::CReflectance()
{
	Initialize();
}

void CReflectance::Initialize()
{
	int k;
	m_bFixed=TRUE; m_bCoated=FALSE; m_bExternal=FALSE;
	m_Fixed = 0; m_Broadband = 0;
	for (k=0; k<MAX_LAYERS; k++) 
		{m_Thick[k]=0; m_Index[k]=1;}
	m_Filename.Empty();
	m_Internal1 = 0; m_Internal2 = 0; m_Rough = PLANAR_SURFACE;
	m_nLambda = 0;
	for (k=0; k<MAX_WAVELENGTHS; k++) m_Lambda[k] = m_Reflectance[k] = 0;
}

void CReflectance::unpack(const ::PC1DGrid::Protocol::ReflectancePtr &reflect)
{
	Initialize();
	
	m_Internal1 = reflect->internalFirstPass;
	m_Internal2 = reflect->internalMultiplePasses;
	
	//Look at m_Rough
	
	switch (reflect->type)
	{
		case ::PC1DGrid::Protocol::kFixedReflectance:
		{
			FixedReflectancePtr fixedReflect = FixedReflectancePtr::dynamicCast(reflect);
			
			m_Fixed = fixedReflect->reflectance;
			m_bFixed = true;
		}
		break;
		
		case ::PC1DGrid::Protocol::kCoatedReflectance:
		{
			CoatedReflectancePtr coatedReflect = CoatedReflectancePtr::dynamicCast(reflect);
			
			m_Broadband = coatedReflect->broadbandReflectance;
			
			//Copy thicknesses
			//FIXME check to make sure we're copying less than MAX_LAYERS
			for (size_t i=0; i<coatedReflect->thicknesses.size(); ++i)
			{
				m_Thick[i] = coatedReflect->thicknesses[i];
				m_Index[i] = coatedReflect->indices[i];
			}
			
			m_bFixed = false;
			m_bCoated = true;
		}
		break;
		
		case ::PC1DGrid::Protocol::kArbitraryReflectance:
		{
			ArbitraryReflectancePtr arbReflect = ArbitraryReflectancePtr::dynamicCast(reflect);
			
			m_nLambda = arbReflect->wavelengths.size();
			UnpackMatchedArrays(arbReflect->wavelengths, arbReflect->reflectances, m_Lambda, m_Reflectance, MAX_WAVELENGTHS);
			
			m_bFixed = false;
			m_bExternal = true;
		}
		break;
	}
}
			
void CReflectance::Serialize(CArchive& ar)
{
	int k, num_layers=MAX_LAYERS;
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		ar << (WORD)m_bCoated << (WORD)m_bExternal;
		ar << CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.ref );		
		ar << m_Fixed << m_Broadband << m_Internal1 << m_Internal2 << (WORD)m_Rough;
		ar << (WORD)num_layers;
		for (k=0; k<num_layers; k++) ar << m_Thick[k] << m_Index[k];
	}
	else
	{
		ar >> (WORD&)m_bCoated >> (WORD&)m_bExternal >> m_Filename;
		ar >> m_Fixed >> m_Broadband >> m_Internal1 >> m_Internal2 >> (WORD&)m_Rough;
		ar >> (WORD&)num_layers;
		for (k=0; k<num_layers; k++) ar >> m_Thick[k] >> m_Index[k];
		m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.ref );
		m_bFixed=!(m_bCoated || m_bExternal);
		m_nLambda = MAX_WAVELENGTHS;
		if (m_bExternal)
			if (!CAscFile::Read( CPath::MakeFullPathname(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.ref),
				m_nLambda, m_Lambda, m_Reflectance))
				AfxMessageBox("Error reading Reflectance file "+m_Filename);
	}
}

/*void CReflectance::TransferDataToDlg(CReflectDlg &dlg, CString OpenDlgTitle)
{
	dlg.m_Radio = 0;
	if (m_bCoated) dlg.m_Radio=1;
	if (m_bExternal) dlg.m_Radio=2;

	dlg.m_Fixed = (100*m_Fixed);	// normalized to percent

	dlg.m_Broadband = (100*m_Broadband);	// normalized to percent
	dlg.m_OuterThick = m_Thick[2];
	dlg.m_OuterIndex = m_Index[2];
	dlg.m_MiddleThick = m_Thick[1];
	dlg.m_MiddleIndex = m_Index[1];
	dlg.m_InnerThick = m_Thick[0];
	dlg.m_InnerIndex = m_Index[0];
	
	dlg.m_Filename = m_Filename;
	dlg.m_Ext = REFLECTANCE_EXT;
	dlg.m_Path = ((CPc1dApp *)AfxGetApp())->m_Path.ref;
	dlg.m_OpenDlgTitle = OpenDlgTitle;
}

void CReflectance::TransferDataFromDlg(CReflectDlg &dlg)
{	
	m_Fixed = (dlg.m_Fixed/100);	// percent to normalized
		
	m_Broadband = (dlg.m_Broadband/100);	// percent to normalized
	m_Thick[2] = dlg.m_OuterThick;
	m_Index[2] = dlg.m_OuterIndex;
	m_Thick[1] = dlg.m_MiddleThick;
	m_Index[1] = dlg.m_MiddleIndex;
	m_Thick[0] = dlg.m_InnerThick;
	m_Index[0] = dlg.m_InnerIndex;
		
	m_Filename = dlg.m_Filename;  

	m_bFixed=m_bCoated=m_bExternal=FALSE;		

	switch (dlg.m_Radio) {
	case 0: m_bFixed=TRUE;
			break; 
	case 1:	m_bCoated=TRUE;
			break;
	case 2:	m_bExternal=TRUE;
			m_nLambda = MAX_WAVELENGTHS;
			if (!CAscFile::Read(m_Filename, m_nLambda, m_Lambda, m_Reflectance))
				AfxMessageBox("Error reading Reflectance file "+m_Filename);
			break;
	}		
}*/


///////////////////////////////////////////////////////////////////////////
// CLumped (Four per Device)

IMPLEMENT_SERIAL(CLumped, CObject, 0)

CLumped::CLumped()
{
	Initialize();
}

void CLumped::Initialize()
{
	m_Enable = FALSE;
	m_Xa = 0; m_Xc = 0; m_n = 1;
	m_Type = CONDUCTOR;
	m_Value = 0;
	for (int k=0; k<MAX_TIME_STEPS+2; k++) m_Volts[k] = m_Amps[k] = 0;
}

void CLumped::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		ar << (WORD)m_Enable << m_Xa << m_Xc << (WORD)m_Type;
		ar << m_n << m_Value;
	}
	else
	{
		ar >> (WORD&)m_Enable >> m_Xa >> m_Xc >> (WORD&)m_Type;
		ar >> m_n >> m_Value;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDevice (One per Document)

IMPLEMENT_SERIAL(CDevice, CObject, 0)

CDevice::CDevice()
{
	pList = new CObList(1);
	Initialize();
}

CDevice::~CDevice()
{
	while (!pList->IsEmpty()) delete (CRegion*)pList->RemoveTail();
	delete pList;
}

void CDevice::Initialize()
{
	while (!pList->IsEmpty()) delete (CRegion*)pList->RemoveTail();
	pR = new CRegion;
	pList->AddTail(pR);
	m_CurrentRegion = 0;
	m_NumRegions = 1;
	m_bModified = FALSE;
	m_Filename.Empty();
	m_FrontRfl.Initialize();
	m_RearRfl.Initialize();
	m_Area = 1;
	m_Aunit = CM2;
	m_FrontTexture = m_RearTexture = FALSE;
	m_FrontAngle = m_RearAngle = 54.74;	// deg
	m_FrontDepth = m_RearDepth = 3e-4;	// cm
	m_FrontSurface = m_RearSurface = NEUTRAL_SURFACE;
	m_FrontBarrier = m_RearBarrier = 0;
	m_FrontCharge = m_RearCharge = 0;
	m_EnableEmitter = TRUE;
	m_EnableBase = TRUE;
	m_EnableCollector = FALSE;
	m_EnableInternal = FALSE;
	m_EmitterR = m_BaseR = m_CollectorR = 1e-6;	// ohms
	m_EmitterX = 0;
	m_BaseX = m_CollectorX = 1;	// cm
	for (int k=0; k<MAX_LUMPED; k++) m_Lumped[k].Initialize();

	SetFileVersion(PC1DVERSION);
}

/////////////////////////////////////////////////////////////////////////////
// CDevice serialization

void CDevice::SetFileVersion(int PC1DVersion)
{
	int i;
	for (i=0; i<m_NumRegions; i++) {
		this->GetMaterialForRegion(i)->SetFileVersion(PC1DVersion);
	}
	m_FileVersion=1;
}

// the earliest version it could be saved as
// This will be the earliest form the material can be saved as.
int CDevice::GetFileVersion()
{
	int ver_num;
	ver_num=41;	//lowest PC1D which actually has a version number
	int i;
	int tmp;
	for (i=0; i<m_NumRegions; i++) {
		tmp=GetMaterialForRegion(i)->GetFileVersion();
		if (tmp>ver_num) ver_num=tmp;
	}
	return ver_num;
}

void CDevice::Serialize(CArchive& ar)
{
	WORD version;
	POSITION pos;
	int i, k, num_lumped=MAX_LUMPED;
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		m_FrontRfl.Serialize(ar);	// Can't be earlier due to Initialize()
		m_RearRfl.Serialize(ar);
		ar << (WORD)num_lumped;
		for (k=0; k<num_lumped; k++) m_Lumped[k].Serialize(ar);
		ar << CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.dev );
		version=DEVICEFILE_VERSION;
		if (version>0) {
			if (m_bModified) ar << (WORD)0xDC01; else ar << (WORD)0xDC00;
			ar << version;
		} else	ar << (WORD)m_bModified;
		// anything after this can use version number. Everything before, we're stuck with!
		ar << (WORD)m_CurrentRegion << (WORD)m_NumRegions;
		pos = pList->GetHeadPosition();
		for (i=0; i<m_NumRegions; i++) pList->GetNext(pos)->Serialize(ar);
		ar << m_Area << (WORD)m_Aunit;
		ar << (WORD)m_FrontTexture << m_FrontAngle << m_FrontDepth;
		ar << (WORD)m_RearTexture << m_RearAngle << m_RearDepth;
		ar << (WORD)m_FrontSurface << m_FrontBarrier << m_FrontCharge;
		ar << (WORD)m_RearSurface << m_RearBarrier << m_RearCharge;
		ar << (WORD)m_EnableEmitter << (WORD)m_EnableBase;
		ar << (WORD)m_EnableCollector << (WORD)m_EnableInternal;
		ar << m_EmitterR << m_BaseR << m_CollectorR;
		ar << m_EmitterX << m_BaseX << m_CollectorX;
	}
	else
	{
		Initialize();	// Necessary to free region objects
		m_FrontRfl.Serialize(ar);	// Must come after Initialize()
		m_RearRfl.Serialize(ar);
		ar >> (WORD&)num_lumped;
		for (k=0; k<num_lumped; k++) m_Lumped[k].Serialize(ar);
		ar >> m_Filename >> (WORD&)m_bModified;
		if ((m_bModified & 0xFF00) == 0xDC00) { ar >> version; m_bModified &=0x00FF;} 
		else version=0;		
		ar >> (WORD&)m_CurrentRegion >> (WORD&)m_NumRegions;
		pR->Serialize(ar);	// Read first region into existing object
		for (i=1; i<m_NumRegions; i++)	// Create any remaining regions
			{pR = new CRegion; pList->AddTail(pR); pR->Serialize(ar);}
		pos = pList->FindIndex(m_CurrentRegion);
		pR = (CRegion*)pList->GetAt(pos);
		ar >> m_Area >> (WORD&)m_Aunit;
		ar >> (WORD&)m_FrontTexture >> m_FrontAngle >> m_FrontDepth;
		ar >> (WORD&)m_RearTexture >> m_RearAngle >> m_RearDepth;
		ar >> (WORD&)m_FrontSurface >> m_FrontBarrier >> m_FrontCharge;
		ar >> (WORD&)m_RearSurface >> m_RearBarrier >> m_RearCharge;
		ar >> (WORD&)m_EnableEmitter >> (WORD&)m_EnableBase;
		ar >> (WORD&)m_EnableCollector >> (WORD&)m_EnableInternal;
		ar >> m_EmitterR >> m_BaseR >> m_CollectorR;
		ar >> m_EmitterX >> m_BaseX >> m_CollectorX;
		m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.dev );
	}
}

void CDevice::Serialize(CArchive& ar, const ::PC1DGrid::Protocol::FileMap &fileMap)
{
	WORD version;
	POSITION pos;
	int i, k, num_lumped=MAX_LUMPED;
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		m_FrontRfl.Serialize(ar);	// Can't be earlier due to Initialize()
		m_RearRfl.Serialize(ar);
		ar << (WORD)num_lumped;
		for (k=0; k<num_lumped; k++) m_Lumped[k].Serialize(ar);
		ar << CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.dev );
		version=DEVICEFILE_VERSION;
		if (version>0) {
			if (m_bModified) ar << (WORD)0xDC01; else ar << (WORD)0xDC00;
			ar << version;
		} else	ar << (WORD)m_bModified;
		// anything after this can use version number. Everything before, we're stuck with!
		ar << (WORD)m_CurrentRegion << (WORD)m_NumRegions;
		pos = pList->GetHeadPosition();
		for (i=0; i<m_NumRegions; i++) pList->GetNext(pos)->Serialize(ar);
		ar << m_Area << (WORD)m_Aunit;
		ar << (WORD)m_FrontTexture << m_FrontAngle << m_FrontDepth;
		ar << (WORD)m_RearTexture << m_RearAngle << m_RearDepth;
		ar << (WORD)m_FrontSurface << m_FrontBarrier << m_FrontCharge;
		ar << (WORD)m_RearSurface << m_RearBarrier << m_RearCharge;
		ar << (WORD)m_EnableEmitter << (WORD)m_EnableBase;
		ar << (WORD)m_EnableCollector << (WORD)m_EnableInternal;
		ar << m_EmitterR << m_BaseR << m_CollectorR;
		ar << m_EmitterX << m_BaseX << m_CollectorX;
	}
	else
	{
		Initialize();	// Necessary to free region objects
		m_FrontRfl.Serialize(ar);	// Must come after Initialize()
		m_RearRfl.Serialize(ar);
		ar >> (WORD&)num_lumped;
		for (k=0; k<num_lumped; k++) m_Lumped[k].Serialize(ar);
		ar >> m_Filename >> (WORD&)m_bModified;
		if ((m_bModified & 0xFF00) == 0xDC00) { ar >> version; m_bModified &=0x00FF;} 
		else version=0;		
		ar >> (WORD&)m_CurrentRegion >> (WORD&)m_NumRegions;
		pR->Serialize(ar, fileMap);	// Read first region into existing object
		for (i=1; i<m_NumRegions; i++)	// Create any remaining regions
			{pR = new CRegion; pList->AddTail(pR); pR->Serialize(ar, fileMap);}
		pos = pList->FindIndex(m_CurrentRegion);
		pR = (CRegion*)pList->GetAt(pos);
		ar >> m_Area >> (WORD&)m_Aunit;
		ar >> (WORD&)m_FrontTexture >> m_FrontAngle >> m_FrontDepth;
		ar >> (WORD&)m_RearTexture >> m_RearAngle >> m_RearDepth;
		ar >> (WORD&)m_FrontSurface >> m_FrontBarrier >> m_FrontCharge;
		ar >> (WORD&)m_RearSurface >> m_RearBarrier >> m_RearCharge;
		ar >> (WORD&)m_EnableEmitter >> (WORD&)m_EnableBase;
		ar >> (WORD&)m_EnableCollector >> (WORD&)m_EnableInternal;
		ar >> m_EmitterR >> m_BaseR >> m_CollectorR;
		ar >> m_EmitterX >> m_BaseX >> m_CollectorX;
		m_Filename=CPath::MinimumNecessaryFilename(m_Filename, ((CPc1dApp *)AfxGetApp())->m_Path.dev );
	}
}


/////////////////////////////////////////////////////////////////////////////
// CDevice functions

double CDevice::GetThickness()
{
	double thick=0;
	// total thickness of device
	for (POSITION pos=pList->GetHeadPosition(); pos!=NULL; ) {
		CRegion *pR =(CRegion*)pList->GetNext(pos);
		thick+=pR->m_Thickness;
	}
	return thick;
}


void CDevice::InsertRegion(int region_num)
{
	CRegion* pRnew = new CRegion;	// allocate space for a new region on the heap
	pRnew->Copy(pR);				// copy contents of current region into it
	pR = pRnew;			// make the new region the current region
	if (region_num>m_NumRegions-1) pList->AddTail(pR);
	else
	{
		POSITION pos = pList->FindIndex(region_num);	// index is zero-based
		pList->InsertBefore(pos, pR);	// add the new region to the linked list
	}
	m_NumRegions++;
	m_CurrentRegion = region_num;
}

void CDevice::RemoveRegion(int region_num)
{
	POSITION pos = pList->FindIndex(region_num);	// Index is zero-based
	ASSERT(pos!=NULL);
	delete (CRegion*)pList->GetAt(pos);
	pList->RemoveAt(pos);
	m_NumRegions--;
	if (m_CurrentRegion+1>m_NumRegions) SetCurrentRegionNumber(m_CurrentRegion-1);
}

void CDevice::SetCurrentRegionNumber(int region_num)
{
	if (region_num>m_NumRegions-1) region_num=m_NumRegions-1; // added by DAC 10/4/96 
	
	POSITION pos = pList->FindIndex(region_num);	// Index is zero-based
	pR = (CRegion*)pList->GetAt(pos);
	m_CurrentRegion = region_num;	
}

BOOL CDevice::OnDeviceOpen()
{
	BOOL FileOpen = TRUE;
	CString Ext = DEVICE_EXT;
	CString Filter = "Device (*."+Ext+")|*."+Ext+"|All Files (*.*)|*.*||";
	CFileDialog dlg(FileOpen, Ext, m_Filename, OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Open Device File";
	dlg.m_ofn.lpstrInitialDir = ((CPc1dApp *)AfxGetApp())->m_Path.dev;	
	if (dlg.DoModal()==IDOK)
	{
		CFile f;
		if (!f.Open(dlg.GetPathName(), CFile::modeRead))
		{
			AfxMessageBox("Unable to open device file");
			return FALSE;
		}
		else 
		{
			CArchive ar(&f, CArchive::load); 
			Serialize(ar);
			m_Filename = dlg.GetPathName();
			m_Filename.MakeLower();
			m_bModified = FALSE;
			return TRUE;
		}
	}
	else return FALSE;
}

void CDevice::OnDeviceSaveas()
{
	CString Ext = DEVICE_EXT;
	CString Filter = "Device (*.dev)|*.dev|PC1D 4.5 Device (*.dev)|*.dev|All Files (*.*)|*.*||";
	CFileDialog dlg(FALSE, Ext, m_Filename, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, Filter);
	dlg.m_ofn.lpstrTitle = "Save Device File";
	dlg.m_ofn.lpstrInitialDir = ((CPc1dApp *)AfxGetApp())->m_Path.dev;	
	if (GetFileVersion()<=46) dlg.m_ofn.nFilterIndex=2;
						else dlg.m_ofn.nFilterIndex=1;
	if (dlg.DoModal()==IDOK)
	{
		CFile f;
		if (!f.Open(dlg.GetPathName(), CFile::modeCreate | CFile::modeWrite))
		{
			AfxMessageBox("Unable to create device file");
		}
		else 
		{
			if (dlg.m_ofn.nFilterIndex==2) {
				SetFileVersion(45);
			} else {
				SetFileVersion(PC1DVERSION);
			}
			CArchive ar(&f, CArchive::store); 
			Serialize(ar);
			m_Filename = dlg.GetPathName();
			m_Filename.MakeLower();
			m_bModified = FALSE;
		}
	}
}

BOOL CDevice::DoAreaDlg()
{
	return TRUE;
}


BOOL CDevice::DoTextureDlg()
{
	return TRUE;
}

BOOL CDevice::DoSurfaceDlg(int toppage)
{
	return TRUE;
}

BOOL	CDevice::DoReflectanceDlg(int toppage)
{
	return TRUE;
}	


BOOL CDevice::DoContactsDlg()
{
	return TRUE;
}

BOOL CDevice::DoInternalElementsDlg()
{
	return TRUE;
}
