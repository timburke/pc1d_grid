/*
 * Required classes for sending a PC1D Material
 */
 
#ifndef __MATERIAL_ICE__
#define __MATERIAL_ICE__

#include <types.ice>
 
module PC1DGrid
{
	module Protocol
	{	
		//Basic type enumerations
		enum ReflectanceType
		{
			kFixedReflectance,
			kCoatedReflectance,
			kArbitraryReflectance
		};
		
		enum MobilityModel
		{
			kFixedMobility,
			kVariableMobility
		};
		
		enum AbsorptionType
		{
			kArbitraryAbsorption
		};
	
		//Reflectance classes
		class Reflectance
		{
			double internalFirstPass; //fraction of *internal* light reflected off the interface on its first pass
			double internalMultiplePasses; //fraction of *internal* light reflected off the interface on subsequent passes
		
			ReflectanceType type;
		};
		
		class FixedReflectance extends Reflectance
		{
			double reflectance; //fraction of light reflected at surface (same for all wavelengths) [0 to 1]
		};
		
		class CoatedReflectance extends Reflectance
		{
			double broadbandReflectance; //fraction all all light reflected at surface (same as Fixed)
			
			//layers must be specified from the substrate (i.e. building them up from the device)
			//There can be at most 3 layers (hardcoded limit in PC1D)
			DoubleArray thicknesses; //layer thicknesses in nm
			DoubleArray indices; //index of refraction of each layer
		};
		
		class ArbitraryReflectance extends Reflectance
		{
			//Sampled reflectance at each wavelength (must not be more than MAX_WAVELENGTHS long
			DoubleArray wavelengths; //nm
			DoubleArray reflectances; //fraction of light at that wavelength that's reflected
		};
		
		struct BGNarrowing
		{
			double onset; //onset doping cm^-3
			double slope; //eV
		};
		
		//The influence of doping on bulk and surface recombination
		struct DopingInfluence
		{
			double nTypeOnset; //cm^-3
			double nTypeAlpha; //unitless, relative to T=300K

			double pTypeOnset; //cm^-3
			double pTypeAlpha; //unitless, relative to T=300K
		};
		
		class Mobility
		{
			MobilityModel model;
		};
		
		class FixedMobility
		{
			double mobility; //cm^2/Vs
		};
		
		class VariableMobility
		{
			double maxMobility; //cm^2/Vs
			
			//Majority carrier
			double minMajority; //cm^2/Vs (minimum mobility) 
			double refConcenMajority; //cm^-3
			double alphaMajority; //unitless ?exponent?
			
			//Minority carrier
			double minMinority; //same unites as above
			double refConcenMinority;
			double alphaMinority;
			
			//thermal model
			double beta1;
			double beta2;
			double beta3;
			double beta4;
			
			double maxVelocity; //cm/s
		};
		
		struct FreeCarrierAbsorption
		{
			bool enable;
			
			double holeCoeff;
			double holeExponent;
			
			double electronCoeff;
			double electronExponent;
		};
		
		class Absorption
		{
			AbsorptionType	type;
		};
		
		class ArbitraryAbsorption extends Absorption
		{
			DoubleArray	wavelengths; //nm
			DoubleArray coefficients; //cm^-1
		};
		
		class Material
		{
			double relativePermittivity; //relative to free space

			double bandGap; //in eV
			double electronAffiniity; //in eV (positive)

			double stateDensityRatio; //Conduction band density of states / Valence band density of states
			
			double ni200; //intrinsic carrier density at T=200K
			double ni300; //intrinsic carrier density at T=300K
			double ni400; //intrinsic carrier density at T=400K
			
			BGNarrowing nTypeNarrowing;
			BGNarrowing pTypeNarrowing;
			
			//Recombination
			DopingInfluence bulkDopingInfluence;
			DopingInfluence surfDopingInfluence;
			
			//Auger
			double nAugerCoeff; //cm^6/s for electrons
			double pAugerCoeff; //cm^6/s for holes
			double highInjectionAugerCoeff; //cm^6/s
			
			//Band-to-Band
			double bandTobandCoeff; //cm^3/s
			
			//Temperature dependence
			double bulkTAlpha;	//tau (in PC1D material dialog) dimensionless relative to 300K
			double surfTAlpha;	 //S (in PC1D material dialog) same as above
			
			//Hurkx Field Enhanced
			bool 	enableHurkx;
			double gamma; //V/cm
			double prefactor; //dimensionless
			
			Mobility	electronMobility;
			Mobility	holeMobility;
			
			//Optical properties
			FreeCarrierAbsorption absFree;
			Absorption			  abs;
			
			//TODO: Finish adding all properties
		};
	};
};

#endif