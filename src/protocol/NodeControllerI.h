/* NodeController.h
* Ice definition files for NodeController servant
*
*/

#ifndef __NodeControllerI_h__

#include "protocol.h"

class NodeControllerI : public virtual PC1DGrid::Control::NodeController
{
	public:
	virtual ::PC1DGrid::NodePrx createNode(const ::Ice::Current&);
};

#endif