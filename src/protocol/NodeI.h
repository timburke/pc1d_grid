/* NodeI.h
 * Defines the Ice implementation of a PC1D problem node.
 *
 */

#ifndef __NodeI_h__
#define __NodeI_h__

#include "protocol.h"

class CProblem;

class NodeI : public virtual PC1DGrid::Node
{
	private:
	CProblem	*problem;
	
	protected:
	virtual ~NodeI();
	
	public:
	
	NodeI();
	
	virtual void setLighting(::PC1DGrid::Protocol::LightPosition pos, const ::PC1DGrid::Protocol::LightPtr&, const ::Ice::Current&);
	virtual void setCircuit(::PC1DGrid::Protocol::CircuitLocation loc, const ::PC1DGrid::Protocol::CircuitPtr &circuit, const ::Ice::Current&);
	virtual void setExcitation(const ::PC1DGrid::Protocol::ExcitationPtr &excitation, const ::Ice::Current&);
	virtual void setSimpleDevice(const ::PC1DGrid::Protocol::SimpleDevicePtr &device, const ::Ice::Current&);
	
	virtual ::PC1DGrid::Protocol::LightPtr getLighting(::PC1DGrid::Protocol::LightPosition pos, const ::Ice::Current&);
	
	virtual void solve(const ::Ice::Current&);
	
	virtual ::PC1DGrid::Protocol::SimulationResults getResults(const ::Ice::Current&);
	virtual ::PC1DGrid::Protocol::QEResults getQuantumEfficiencies(const ::Ice::Current&);
	virtual ::PC1DGrid::Protocol::OpticalResults getOpticalResults(const ::Ice::Current&);
};

#endif