/* NodeI.cpp
 *
 *
 */

#include "stdafx.h"
#include "NodeI.h"
#include "Problem.h"
#include <iostream>
#include <vector>
#include "Useful.h"
#include "PhysicalConstants.h"
#include <math.h>

using namespace std;
using namespace PC1DGrid;

NodeI::NodeI(void) : problem(NULL)
{
	problem = new CProblem();
	problem->Initialize();
}

NodeI::~NodeI()
{
	if (problem)
		delete problem;
}

void NodeI::setLighting(::PC1DGrid::Protocol::LightPosition pos, const ::PC1DGrid::Protocol::LightPtr &light, const ::Ice::Current&)
{
	if (pos == ::PC1DGrid::Protocol::kFrontPosition)	
		problem->GetExcite()->m_LightPri.unpack(light);
	else
		problem->GetExcite()->m_LightSec.unpack(light);		
}

::PC1DGrid::Protocol::SimulationResults NodeI::getResults(const ::Ice::Current&)
{
	Protocol::SimulationResults results;
	
	CopyArray(problem->GetExcite()->m_Base.m_Volts, results.baseVoltage, problem->GetExcite()->m_TranNum);
	CopyArray(problem->GetExcite()->m_Base.m_Amps, results.baseCurrent, problem->GetExcite()->m_TranNum);
	CopyArray(problem->m_Time, results.time, problem->GetExcite()->m_TranNum);
	
	return results;
}

::PC1DGrid::Protocol::QEResults NodeI::getQuantumEfficiencies(const ::Ice::Current&)
{
	Protocol::QEResults results;
	
	int waveCount = problem->GetExcite()->m_TranNum;
	
	results.wavelengths.resize(waveCount);
	results.eqe.resize(waveCount);
	results.iqe.resize(waveCount);
	
	CExcite *pE=problem->GetExcite();
	double area = problem->GetAref();
	
	//The information is stored in an array of length MAX_TIME_STEPS+2
	//Where the 0th entry corresponds to the ss value and the last entry is a repeat of the max value
	for (int i=1; i<=waveCount; ++i) 
	{
		results.wavelengths[i-1] = pE->m_LightPri.m_Lambda[i];
		
		if (pE->m_LightPri.m_Intensity[i]>0 && pE->m_LightPri.m_Lambda[i]>0)
		{
			results.eqe[i-1] = HC/pE->m_LightPri.m_Lambda[i] *
					fabs(pE->m_Base.m_Amps[i])/
					(area*pE->m_LightPri.m_Intensity[i]);
					
			results.iqe[i-1] = results.eqe[i-1] / ((1.0 - pE->m_LightPri.m_Reflectance[i])
							* (1.0 - pE->m_LightPri.m_Escape[i]));
		}
		else
		{
			results.eqe[i-1] = 0.0;
			results.iqe[i-1] = 0.0;
		}
	}
	
	return results;
}
	
::PC1DGrid::Protocol::OpticalResults NodeI::getOpticalResults(const ::Ice::Current&)
{
	Protocol::OpticalResults results;

	CExcite *pE=problem->GetExcite();
	
	int waveCount = problem->GetExcite()->m_TranNum;
	
	results.wavelengths.resize(waveCount);
	results.transmission.resize(waveCount);
	
	for (int i=1; i<=waveCount; ++i) 
	{
		results.wavelengths[i-1] = pE->m_LightPri.m_Lambda[i];
		results.transmission[i-1] = pE->m_LightPri.m_Transmitted[i];
	}
	
	return results;
}

void NodeI::solve(const ::Ice::Current&)
{
	CFile outparams("C:\\device.prm", CFile::modeWrite | CFile::modeCreate | CFile::shareExclusive);
	CArchive outArchive(&outparams, CArchive::store);

	problem->Serialize(outArchive);
	problem->StartNewCalculation();	
	while (!problem->IsProblemCompletelySolved())
		problem->DoNextPartOfCalculation(NULL);
}

void NodeI::setSimpleDevice(const ::PC1DGrid::Protocol::SimpleDevicePtr &device, const ::Ice::Current&)
{
	size_t length = device->deviceFile.size();

	BYTE *buffer = new BYTE[length];

	for (size_t i=0; i<length; ++i)
		buffer[i] = device->deviceFile[i];

	CMemFile deviceFile(buffer, length, 0); //Don't allow growing since we didn't allocate this memory
	
	CArchive deviceArchive(&deviceFile, CArchive::load);
	
	problem->GetDevice()->Serialize(deviceArchive, device->externalFiles);
	
	deviceFile.Detach(); //Make sure we don't attempt to free this memory when the memfile goes out of scope.
	delete[] buffer;
}	

void NodeI::setCircuit(Protocol::CircuitLocation loc, const Protocol::CircuitPtr &circuit, const ::Ice::Current&)
{
	if (loc == Protocol::kCollectorCircuit)
		problem->GetExcite()->m_Coll.unpack(circuit);
	else
		problem->GetExcite()->m_Base.unpack(circuit);
}

void NodeI::setExcitation(const Protocol::ExcitationPtr &excitation, const ::Ice::Current&)
{
	problem->GetExcite()->unpack(excitation);
}

::PC1DGrid::Protocol::LightPtr NodeI::getLighting(::PC1DGrid::Protocol::LightPosition pos, const ::Ice::Current&)
{	
	if (pos == ::PC1DGrid::Protocol::kFrontPosition)
		return problem->GetExcite()->m_LightPri.pack();
	else
		return problem->GetExcite()->m_LightSec.pack();
}
