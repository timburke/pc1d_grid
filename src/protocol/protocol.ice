/*pc1d_grid.ice
This file defines the basic types and API for communicating with a PC1D grid
node.
*/

#include <material.ice>
#include <excitation.ice>
#include <simpledevice.ice>

module PC1DGrid
{
	//Current problem limits as of PC1D 5.3
	module Constants 
	{
		const int kMaxWavelengths = 200;
		const int kMaxTimesteps = 200;
	};
	
	module Protocol
	{	
		struct SampledFunction
		{
			DoubleArray x;
			DoubleArray	y;
		};
		
		struct SimulationResults
		{
			DoubleArray time;
			DoubleArray	baseCurrent;
			DoubleArray baseVoltage;
		};
		
		struct QEResults
		{
			DoubleArray wavelengths;
			
			DoubleArray eqe;
			DoubleArray iqe;
		};
		
		struct OpticalResults
		{
			DoubleArray wavelengths;
			DoubleArray transmission;
		};
	};
	
	interface Node
	{
		//Operations for setting problem information
		void setLighting(Protocol::LightPosition position, Protocol::Light light) throws Protocol::ParameterError;
		void setCircuit(Protocol::CircuitLocation loc, Protocol::Circuit circuit) throws Protocol::ParameterError;
		void setExcitation(Protocol::Excitation excitation) throws Protocol::ParameterError;
		void setSimpleDevice(Protocol::SimpleDevice device) throws Protocol::ParameterError;
		
		//Operations for getting problem information		
		Protocol::Light getLighting(Protocol::LightPosition position);
		
		//Solving
		void solve();
		
		//Obtaining results
		Protocol::SimulationResults getResults();
		Protocol::QEResults			getQuantumEfficiencies();
		Protocol::OpticalResults	getOpticalResults();
	};
	
	module Control
	{
		interface NodeController
		{
			Node* createNode();
		};
	};
};