/* NodeControllerI.cpp
 * Implementatation of NodeController ICE object that manages creating and destroying
 * PC1D Problem instances (Node objects)
 */

#include "NodeControllerI.h"
#include "NodeI.h"
#include <Ice/Ice.h>
#include <IceUtil/UUID.h>

using namespace PC1DGrid;

typedef IceUtil::Handle<NodeI> NodeIPtr;

NodePrx NodeControllerI::createNode(const ::Ice::Current &current)
{
	//Create a node object, add it to the object adapter and return a proxy to our caller
	NodeIPtr newNode = new NodeI();
	
	NodePrx proxy = NodePrx::uncheckedCast(current.adapter->addWithUUID(newNode));

	return proxy;
}