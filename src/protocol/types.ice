/*
 * types.h - Defines common types for the protocol and exceptions
 */
 
#ifndef __TYPES_ICE__
#define __TYPES_ICE__

module PC1DGrid
{
	module Protocol
	{
		//Exceptions
		exception ParameterError 
		{
			string reason;
			string parameterName = "Not Given";
		};
		
		sequence<double> 				DoubleArray;
		sequence<byte> 					ByteArray;
	};
};

#endif