/*
 * Required classes for sending PC1D Excitation Information
 */
 
#ifndef __EXCITATION_ICE__
#define __EXCITATION_ICE__

#include <types.ice>
 
module PC1DGrid
{
	module Protocol
	{	
		enum LightType
		{
			kMonochromatic,
			kBlackbody,
			kArbitrary
		};
		
		enum LightPosition
		{
			kFrontPosition,
			kBackPosition
		};
		
		enum ResistanceUnits
		{
			kResistance,
			kResistivity
		};
		
		enum CircuitLocation
		{
			kCollectorCircuit,
			kBaseCircuit
		};
		
		enum ExcitationType
		{
			kEquilibrium,
			kSteadyState,
			kTransient
		};
		
		class Circuit
		{
			bool arbitrary; //Is this specified in an array or simply using the below parameters
			bool connectedSteadyState = true;
			bool connectedTransient = true;
			
			ResistanceUnits units;
			
			double startingVoltage;
			double endingVoltage = 0.0;
			
			double resistance; //Same for transient and steadystate excitation
			
			//For arbitrary circuits, specify the R and V values in ohms and volts
			//sampled at each time (seconds).  Must be <= MAX_TIME_STEPS
			DoubleArray time;
			DoubleArray voltageDynamic;
			DoubleArray resistanceDynamic;
		};
		
		class Light 
		{
			bool enabled = false; //Is this light enabled?
			bool steadyState = true; //Is this light (not) going to change transiently?

			LightType 		type; //How is this light defined?
			LightPosition	position; //Is this light behind or in front of the device?
			
			double startingIntensity; 	//W/cm^2 the scaling factor applied to the light spectrums given below
			double endingIntensity = 0.0; //W/cm^2
		};
		
		class MonochromaticLight extends Light
		{
			//All the information needed for specifying a monochromatic light source
			//If not doing a transient analysis, only startingWavelength is used
			double startingWavelength; //in nm
			double endingWavelength = 0.0; //in nm
		};
		
		class BlackbodyLight extends Light
		{
			//All the information needed for specifying a blackbody light source
			double 	temperature; //In Kelvin
		
			double 	minWavelength; //in nm, the lowest wavelength to include in spectrum
			double 	maxWavelength = 0.0; //in nm, the highest wavelength to include in spectrum
			int 	numWavelengths; //The number of divisions to make between minWavelength and maxWavelength
		};
				
		class ArbitraryLight extends Light
		{
			//The sequence must be shorter the PC1D's MAX_WAVELENGTHS limit
			DoubleArray wavelengths; //nm, the wavelengths at which the spectrum is sampled
			DoubleArray intensities; //W/cm^2 the arbitrarily normalized intensities for this spectrum
		};
		
		
		//Excitation classes
		class Excitation
		{
			Circuit baseCircuit;
			Circuit collectorCircuit;
			
			ExcitationType type;
			
			double temperature; //Device Temp in Kelvin
			
			Light frontLight;
			Light backLight;
		};
		
		class EquilibriumExcitation extends Excitation
		{
		
		};
		
		class SteadyStateExcitation extends Excitation
		{
		
		};
		
		class TransientExcitation extends Excitation
		{
			int numSteps; //The number of timesteps to iterate
			double stepLength; //The length of each step in seconds
			double firstStepLength = 0.0; //The length of the first step in seconds
		};
	};
};

#endif