/*
 * Required classes for sending a PC1D device file with external materials to a grid node
 * does not allow editing of the device parameters, just passing an externally created
 * PC1D device file with the supporting material specifications
 */
 
#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <types.ice>
 
module PC1DGrid
{
	module Protocol
	{	
		struct ExternalFile
		{
			DoubleArray x;
			DoubleArray y;
		};
		
		dictionary<string, ExternalFile> FileMap;
		
		class SimpleDevice
		{
			["python:seq:list"] ByteArray	deviceFile; //A byte for byte copy of the device file
			
			FileMap 	externalFiles; //A list of filename -> struct ExternalFile for specifying properties that were defined externally to the device file
		};
	};
};

#endif